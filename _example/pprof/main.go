package main

import (
	"flag"
	"fmt"
	"net/http"
	_ "net/http/pprof"
)

var port int

func main() {
	flag.IntVar(&port, "port", 6060, "Port to serve on")
	flag.Parse()

	fmt.Printf("Listening on http://localhost:%d\n", port)
	if err := http.ListenAndServe(fmt.Sprintf(":%d", port), nil); err != nil {
		panic(err)
	}
}
