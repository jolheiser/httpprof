package main

import (
	"flag"
	"fmt"
	"net/http"

	"go.jolheiser.com/httpprof"
)

var port int

func main() {
	flag.IntVar(&port, "port", 6060, "Port to serve on")
	flag.Parse()

	fmt.Printf("Listening on http://localhost:%d\n", port)
	if err := http.ListenAndServe(fmt.Sprintf(":%d", port), httpprof.Handler()); err != nil {
		panic(err)
	}
}
