package httpprof

import (
	_ "embed"
	"net/http"
	"net/http/pprof"
	"runtime"
	"strconv"
	"strings"
)

// Handler wraps pprof endpoints
func Handler(enablePOST bool) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		// pprof wrapper
		if r.Method == http.MethodGet {
			switch r.URL.Path {
			case "/":
				injectJS(w)
				pprof.Index(w, r)
			case "/cmdline":
				pprof.Cmdline(w, r)
			case "/profile":
				pprof.Profile(w, r)
			case "/symbol":
				pprof.Symbol(w, r)
			case "/trace":
				pprof.Trace(w, r)
			default:
				pprof.Handler(strings.TrimPrefix(r.URL.Path, "/")).ServeHTTP(w, r)
			}
			return
		}

		// convenience wrappers
		if enablePOST && r.Method == http.MethodPost {
			var rate int
			rateParam := r.FormValue("rate")
			if rateParam != "" {
				rate, _ = strconv.Atoi(rateParam)
			}
			switch r.URL.Path {
			case "block":
				runtime.SetBlockProfileRate(rate)
				w.WriteHeader(http.StatusOK)
			case "mutex":
				runtime.SetMutexProfileFraction(rate)
				w.WriteHeader(http.StatusOK)
			default:
				w.WriteHeader(http.StatusNotFound)
			}
		}
	}
}

var (
	//go:embed httpprof.js
	js     string
	script = []byte(`<script>` + js + `</script>`)
)

func injectJS(w http.ResponseWriter) {
	// Explicitly set content type as HTML
	w.Header().Set("Content-Type", "text/html; charset=utf-8")
	// Inject JS
	_, _ = w.Write(script)
}
