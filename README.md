# httpprof

A `net/http/pprof` wrapper as a handler.

Includes two `POST` targets, `block` and `mutex`
that run `runtime.SetBlockProfileRate` and `runtime.SetMutexProfileFraction` respectively.

## License

[MIT](LICENSE)